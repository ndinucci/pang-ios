//
//  GameOver.m
//  Pang
//
//  Created by Melisa Anabella Rossi on 20/6/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameOver.h"
#import "LevelOne.h"

@implementation GameOver


-(void) onEnter{
    [super onEnter];
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *currentscorekey = @"currentscore";
    NSString *highestscorekey = @"highestscore";
    NSString *maxcombokey = @"maxCombo";
    
    CCTime currentscore = [preferences integerForKey:currentscorekey];
    CCTime highestscore = [preferences integerForKey:highestscorekey];
    NSInteger maxcombo = [preferences integerForKey:maxcombokey];
    
    if (!highestscore || currentscore > highestscore){
        highestscore = currentscore;
        [preferences setInteger:highestscore forKey:highestscorekey];
    }
    
    _score.string = [NSString stringWithFormat:@"SCORE: %0.f\nHIGHSCORE: %0.f\nMAX COMBO: %ld", currentscore,highestscore,maxcombo];

}

-(void) retry{
    CCScene *gameplayScene = [CCBReader loadAsScene:@"LevelOne"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
}

@end
