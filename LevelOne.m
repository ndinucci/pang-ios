//
//  LevelOne.m
//  Pang
//
//  Created by Melisa Anabella Rossi on 5/6/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "LevelOne.h"

@implementation LevelOne
{
    int direction;
    CCTime score;
    Boolean shotInProgress;
    Boolean missileSpeedBuff;
    Boolean playerSpeedBuff;
    Boolean invaderAlive;
    CCTime missileSpeedBuffRemaining;
    CCTime playerSpeedBuffRemaining;
    CCTime nextInvader;
    id<ALSoundSource> walking;
    CCLabelTTF* _score;
    int comboCounter;
    int maxCombo;
    CCTime comboTimeRemaining;
    Boolean playerSpeedDebuff;
    CCTime playerSpeedDebuffRemaining;
    int maxInvaderHealth;
    int currentInvaderHealth;
}

-(void) onEnter{
    [super onEnter];
    score = 0.0;
    shotInProgress = false;
    self.userInteractionEnabled = YES;
    direction=0;
    _physicsNode.gravity = CGPointMake( 0 , -200);
    _physicsNode.collisionDelegate = self;
    _balls = [[CCNode alloc ]init];
    [_physicsNode addChild:_balls];
    [self schedule:@selector(addBall:) interval:10];
    [self schedule:@selector(addBuff:) interval:13];
    [self schedule:@selector(addInvaderBall:) interval:4];
    [self addBall: 2.0];
    invaderAlive = false;
    missileSpeedBuffRemaining = 0.0;
    nextInvader = 13;
    playerSpeedBuffRemaining = 0.0;
    [_comboLabel setVisible:false];
    [_invaderHealthLabel setVisible:false];
    comboCounter = 0;
    maxCombo = 0;
    maxInvaderHealth = 0;
    currentInvaderHealth = 0;

}

-(void) mainmenu{
    CCScene *gameplayScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
}


- (void)addBuff:(CCTime)dt {
    int buffNumber = (arc4random() % 6);
    int posx = (arc4random() % 450) + 50;
    CCNode* buff;
    switch (buffNumber) {
        case 0:{
            buff = [CCBReader load:@"PlayerSpeedBuff"];
            break;
        }
        case 1:{
            buff = [CCBReader load:@"MissileSpeedBuff"];
            break;
        }
        case 2:{
            buff = [CCBReader load:@"BallDestroyer"];
            break;
        }
        case 3:{
            buff = [CCBReader load:@"PlayerSpeedDebuff"];
            break;
        }
        case 4:{
            buff = [CCBReader load:@"AddBallDebuff"];
            break;
        }
        case 5:{
            buff = [CCBReader load:@"RandomBuff"];
            break;
        }
    }
    buff.position = CGPointMake(posx, _player.position.y);
    [_physicsNode addChild:buff];
    buff.physicsBody.collisionGroup = @"ballGroup";
}

- (void)addBall:(CCTime)dt {
    CCNode* ball = [CCBReader load:@"BigBall"];
    int posx = (arc4random() % 450) + 50;
    int velx = (arc4random() % 400) - 200;
    int vely = (arc4random() % 40) - 20;
    ball.physicsBody.collisionGroup = @"ballGroup";
    ball.position =  CGPointMake(posx, 250);
    [_balls addChild:ball];
    ball.physicsBody.velocity = CGPointMake(velx, vely);

}

- (void)addInvaderBall:(CCTime)dt {
    if (invaderAlive){
        CCNode* ball = [CCBReader load:@"Ball"];
        ball.physicsBody.collisionGroup = @"ballGroup";
        ball.position =  _invader.position;
        [_balls addChild:ball];
        ball.physicsBody.velocity = _invader.physicsBody.velocity;
    }
}

- (void)addInvader {
    maxInvaderHealth++ ;
    currentInvaderHealth = maxInvaderHealth;
    _invader = [CCBReader load:@"Invader"];
    int posx = (arc4random() % 450) + 50;
    _invader.physicsBody.collisionGroup = @"ballGroup";
    _invader.position =  CGPointMake(posx, 270);
    invaderAlive = true;
    [_physicsNode addChild:_invader];
    _invader.physicsBody.velocity = CGPointMake(200, 0);
    [_invaderHealthLabel setVisible: true];
    _invaderHealthLabel.string = [NSString stringWithFormat:@"INVADER HEALTH: %d", currentInvaderHealth];
    
}

- (void)applyPlayerSpeedBuff {
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/PLAYER SPEED.wav"];
    playerSpeedBuff = true;
    playerSpeedBuffRemaining = 10.0;
}

- (void)applyMissileSpeedBuff {
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/MISSILE BUFF.wav"];
    missileSpeedBuff = true;
    missileSpeedBuffRemaining = 10.0;
}

- (void)applyBallDestroyer {
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/DESTROY BALLS.wav"];
    [_balls removeAllChildren];
}

- (void)applyPlayerSpeedDebuff {
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/PLAYER SPEED DEBUFF.wav"];
    playerSpeedDebuff = true;
    playerSpeedDebuffRemaining = 10.0;
}

- (void)applyAddBallDebuff {
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/ADD BALL DEBUFF.wav"];
    [self addBall: 2.0];
}

-(void) shoot{
    if(!shotInProgress){
        CCNode* projectile = [CCBReader load:@"Projectile"];
        projectile.position =  CGPointMake(_player.position.x, _player.position.y +10);
        [_physicsNode addChild:projectile];
        OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
        [audio playEffect:@"sound/SHOOT.wav"];
        if(missileSpeedBuff){
            projectile.physicsBody.velocity = CGPointMake(0, 1000);
        } else {
            projectile.physicsBody.velocity = CGPointMake(0, 500);
        }
        shotInProgress = TRUE;
    }
}


-(void) touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    if(touch.locationInWorld.y < 100 ){
        OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
        walking = [audio playEffect:@"sound/WALK.wav" loop:YES];
        if(touch.locationInWorld.x > 285){
            direction=1;
        } else{
            direction=-1;
        }
    } else {
        self.shoot;
    }
}

-(void) touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    direction=0;
    [walking stop];
}

-(void) update:(CCTime)delta{
    int speed = 100;
    if(playerSpeedBuff){
        speed += 50 ;
    }
    if(playerSpeedDebuff){
        speed -= 50 ;
    }
    _player.position = ccpAdd(_player.position, CGPointMake((delta*direction*speed), 0));
    if(playerSpeedBuffRemaining != 0){
        if(delta > playerSpeedBuffRemaining){
            playerSpeedBuffRemaining = 0;
            playerSpeedBuff = false;
        } else {
            playerSpeedBuffRemaining -= delta;
        }
    }
    if(playerSpeedDebuffRemaining != 0){
        if(delta > playerSpeedDebuffRemaining){
            playerSpeedDebuffRemaining = 0;
            playerSpeedDebuff = false;
        } else {
            playerSpeedDebuffRemaining -= delta;
        }
    }
    if(missileSpeedBuffRemaining != 0){
        if(delta > missileSpeedBuffRemaining){
            missileSpeedBuffRemaining = 0;
            missileSpeedBuff = false;
        } else {
            missileSpeedBuffRemaining -= delta;
        }
    }
    if(comboTimeRemaining != 0){
        if(delta > comboTimeRemaining){
            comboTimeRemaining = 0;
            [_comboLabel setVisible:false];
        } else {
            comboTimeRemaining -= delta;
        }
    }
    if(nextInvader != 0){
        if(delta > nextInvader){
            nextInvader = 0;
            [self addInvader];
        } else {
            nextInvader -= delta;
        }
    }
    score+=delta;
    _scoreLabel.string = [NSString stringWithFormat:@"%.0f", score];
    
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair ballCollision:(CCNode *)ball playerCollision:(CCNode *)player {
    [player removeFromParent];
    [ball removeFromParent];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/GAME OVER.wav"];
    _score.string = [NSString stringWithFormat:@"SCORE: "];
    CCScene *gameplayScene = [CCBReader loadAsScene:@"GameOver"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *currentscorekey = @"currentscore";
    NSString *maxcombokey = @"maxCombo";
    
    CCTime currentscore = [preferences integerForKey:currentscorekey];
    
    currentscore = score;
    [preferences setInteger:maxCombo forKey:maxcombokey];
    [preferences setInteger:currentscore forKey:currentscorekey];
    return YES;
}


-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair littleBallCollision:(CCNode *)ball playerCollision:(CCNode *)player {
    [player removeFromParent];
    [ball removeFromParent];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/GAME OVER.wav"];
    CCScene *gameplayScene = [CCBReader loadAsScene:@"GameOver"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *currentscorekey = @"currentscore";
    NSString *maxcombokey = @"maxCombo";
    
    CCTime currentscore = [preferences integerForKey:currentscorekey];
    
    currentscore = score;
    [preferences setInteger:maxCombo forKey:maxcombokey];
    [preferences setInteger:currentscore forKey:currentscorekey];
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair ballCollision:(CCNode *)ball projectileCollision:(CCNode *)projectile {
    [projectile removeFromParent];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/BREAK BUBBLE.wav"];
    shotInProgress = false;
    [ball removeFromParent];
    //first little ball
    CCNode* lball1 = [CCBReader load:@"Ball"];
    int velx = (arc4random() % 100) + 100;
    int vely = (arc4random() % 200) + 200;
    lball1.physicsBody.collisionGroup = @"ballGroup";
    lball1.position =  ball.position;
    [_balls addChild:lball1];
    lball1.physicsBody.velocity = CGPointMake(velx, vely);
    //second little ball
    CCNode* lball2 = [CCBReader load:@"Ball"];
    lball2.physicsBody.collisionGroup = @"ballGroup";
    lball2.position =  ball.position;
    [_balls addChild:lball2];
    lball2.physicsBody.velocity = CGPointMake(-velx, vely);
    // crear 2 ball , misma pos , 1 a la izq una a la der , velocidad en y , hacia arriba
    
    comboCounter++;
    if(maxCombo < comboCounter){
        maxCombo = comboCounter;
    }
    _comboLabel.string = [NSString stringWithFormat:@"Combo x%d",comboCounter];
    if (comboCounter > 2){
        [_comboLabel setVisible:true];
        comboTimeRemaining = 1;
    } else {
        [_comboLabel setVisible: false];
    }
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair littleBallCollision:(CCNode *)ball projectileCollision:(CCNode *)projectile {
    [projectile removeFromParent];
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio playEffect:@"sound/BREAK BUBBLE.wav"];
    shotInProgress = false;
    [ball removeFromParent];
    
    comboCounter++;
    if(maxCombo < comboCounter){
        maxCombo = comboCounter;
    }
    _comboLabel.string = [NSString stringWithFormat:@"Combo x%d",comboCounter];
    if (comboCounter > 2){
        [_comboLabel setVisible:true];
        comboTimeRemaining = 1;
    } else {
        [_comboLabel setVisible: false];
    }
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair roofCollision:(CCNode *)roof projectileCollision:(CCNode *)projectile {
    [projectile removeFromParent];
    shotInProgress = false;
    comboCounter = 0;
    _comboLabel.string = [NSString stringWithFormat:@"Miss"];
    [_comboLabel setVisible:true];
    comboTimeRemaining = 1;
    
    
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair invaderCollision:(CCNode *)invader projectileCollision:(CCNode *)projectile {
    [projectile removeFromParent];
    shotInProgress = false;
    currentInvaderHealth-- ;
    if(currentInvaderHealth == 0){
        [invader removeFromParent];
        invaderAlive = false;
        nextInvader = 20;
        OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
        [audio playEffect:@"sound/INVADER KILL.wav"];
        [ _invaderHealthLabel setVisible: false];
    } else {
        _invaderHealthLabel.string = [NSString stringWithFormat:@"INVADER HEALTH: %d", currentInvaderHealth];
        OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
        [audio playEffect:@"sound/INVADER HIT.wav"];
    }
    
    
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair missileSpeedBuffCollision:(CCNode *)buff playerCollision:(CCNode *)player {
    [buff removeFromParent];
    [self applyMissileSpeedBuff];
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerSpeedBuffCollision:(CCNode *)buff playerCollision:(CCNode *)player {
    [buff removeFromParent];
    [self applyPlayerSpeedBuff];
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair ballDestroyerCollision:(CCNode *)destroyer playerCollision:(CCNode *)player {
    [destroyer removeFromParent];
    [self applyBallDestroyer];
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerSpeedDebuffCollision:(CCNode *)debuff playerCollision:(CCNode *)player {
    [debuff removeFromParent];
    [self applyPlayerSpeedDebuff];
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair addBallDebuffCollision:(CCNode *)debuff playerCollision:(CCNode *)player {
    [debuff removeFromParent];
    [self applyAddBallDebuff];
    return YES;
}

-(BOOL) ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair randomBuffCollision:(CCNode *)buff playerCollision:(CCNode *)player {
    [buff removeFromParent];
    int buffNumber = (arc4random() % 5);
    switch (buffNumber) {
        case 0:{
            [self applyMissileSpeedBuff];
            break;
        }
        case 1:{
            [self applyPlayerSpeedBuff];
            break;
        }
        case 2:{
            [self applyBallDestroyer];
            break;
        }
        case 3:{
            [self applyPlayerSpeedDebuff];
            break;
        }
        case 4:{
            [self applyAddBallDebuff];
            break;
        }
    }
    return YES;
    
}

@end
