//
//  LevelOne.h
//  Pang
//
//  Created by Melisa Anabella Rossi on 5/6/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "Player.h"
#import "BigBall.h"
#import "Projectile.h"


@interface LevelOne : CCNode <CCPhysicsCollisionDelegate>

@property (nonatomic,strong) CCPhysicsNode* physicsNode;
@property (nonatomic,strong) Player* player;
@property (nonatomic,strong) CCNode* balls;
@property (nonatomic,strong) CCLabelTTF* scoreLabel;
@property (nonatomic,strong) CCLabelTTF* comboLabel;
@property (nonatomic,strong) CCLabelTTF* invaderHealthLabel;
@property (nonatomic,strong) CCNode* invader;



@end

