//
//  GameOver.h
//  Pang
//
//  Created by Melisa Anabella Rossi on 20/6/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface GameOver : CCNode

@property (nonatomic,strong) CCLabelTTF* score;


@end

